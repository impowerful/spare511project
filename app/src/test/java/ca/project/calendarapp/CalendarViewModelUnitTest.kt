package ca.project.calendarapp

import ca.project.calendarapp.ui.theme.CalendarViewModel
import ca.project.calendarapp.ui.theme.Event
import org.junit.Assert.assertEquals
import org.junit.Test

class CalendarViewModelTest {

    @Test
    fun addEvent_updatesUiStateWithNewEvent() {
        // Arrange
        val viewModel = CalendarViewModel()
        val event = Event("Title", "Location", "Description", null, null, null, 1)

        // Act
        viewModel.addEvent(event)

        // Assert
        assertEquals(listOf(event), viewModel.uiState.value.events)
    }

    @Test
    fun updateEvent_updatesUiStateWithUpdatedEvent() {
        // Arrange
        val viewModel = CalendarViewModel()
        val existingEvent = Event("Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(existingEvent)
        val updatedEvent = existingEvent.copy(title = "Updated Title")

        // Act
        viewModel.updateEvent(existingEvent.id ?: -1, updatedEvent)

        // Assert
        assertEquals(listOf(updatedEvent), viewModel.uiState.value.events)
    }

    @Test
    fun deleteEvent_removesEventFromUiState() {
        // Arrange
        val viewModel = CalendarViewModel()
        val eventToDelete = Event("Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(eventToDelete)

        // Act
        viewModel.deleteEvent(eventToDelete.id ?: -1)

        // Assert
        assertEquals(emptyList<Event>(), viewModel.uiState.value.events)
    }


    @Test
    fun deleteEvent_withNonExistentId_doesNotModifyUiState() {
        // Arrange
        val viewModel = CalendarViewModel()
        val initialEvent = Event("Initial Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(initialEvent)

        // Act
        viewModel.deleteEvent(999) // Non-existent ID

        // Assert
        assertEquals(listOf(initialEvent), viewModel.uiState.value.events)
    }

    @Test
    fun updateEvent_withNonExistentId_doesNotModifyUiState() {
        // Arrange
        val viewModel = CalendarViewModel()
        val initialEvent = Event("Initial Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(initialEvent)
        val updatedEvent = initialEvent.copy(title = "Updated Title")

        // Act
        viewModel.updateEvent(999, updatedEvent) // Non-existent ID

        // Assert
        assertEquals(listOf(initialEvent), viewModel.uiState.value.events)
    }

    @Test
    fun updateEvent_withValidId_updatesUiState() {
        // Arrange
        val viewModel = CalendarViewModel()
        val initialEvent = Event("Initial Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(initialEvent)
        val updatedEvent = initialEvent.copy(title = "Updated Title")

        // Act
        viewModel.updateEvent(initialEvent.id ?: -1, updatedEvent)

        // Assert
        assertEquals(listOf(updatedEvent), viewModel.uiState.value.events)
    }

    @Test
    fun deleteEvent_withEmptyUiState_doesNotModifyUiState() {
        // Arrange
        val viewModel = CalendarViewModel()

        // Act
        viewModel.deleteEvent(1) // Attempt to delete from an empty UI state

        // Assert
        assertEquals(emptyList<Event>(), viewModel.uiState.value.events)
    }

    @Test
    fun deleteEvent_withInvalidId_doesNotModifyUiState() {
        // Arrange
        val viewModel = CalendarViewModel()
        val initialEvent = Event("Title", "Location", "Description", null, null, null, 1)
        viewModel.addEvent(initialEvent)

        // Act
        viewModel.deleteEvent(-1) // Attempt to delete with an invalid ID

        // Assert
        assertEquals(listOf(initialEvent), viewModel.uiState.value.events)
    }
}
