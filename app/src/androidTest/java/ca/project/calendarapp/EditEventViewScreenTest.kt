package ca.project.calendarapp

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import ca.project.calendarapp.ui.theme.CalendarViewModel
import ca.project.calendarapp.ui.theme.EditEventScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditEventScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun editEventScreenRendersCorrectly() {
        // Given
        composeTestRule.setContent {
            EditEventScreen(
                onSaveButtonClicked = { },
                calendarViewModel = CalendarViewModel()
            )
        }

        // Allow time for rendering
        Thread.sleep(1000)

        // Then
        composeTestRule.onNodeWithText("Back").assertExists()
        composeTestRule.onNodeWithText("Save Changes").assertExists()
    }
}
