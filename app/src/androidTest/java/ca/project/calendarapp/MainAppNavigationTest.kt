package ca.project.calendarapp

import androidx.activity.ComponentActivity
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import ca.project.calendarapp.ui.theme.CalendarViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick


class MainAppNavigationTest {
    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    private lateinit var navController: TestNavHostController

    @Before
    fun setupCalendarNavHost() {
        composeTestRule.setContent {
            navController = TestNavHostController(LocalContext.current).apply {
                navigatorProvider.addNavigator(ComposeNavigator())
            }
            MainApp(
                calendarViewModel = CalendarViewModel(),
                navController = navController,
                modifier = Modifier
            )
        }
    }
    
    @Test
    fun calendarNavHost_verifyStartDestination(){
        navController.assertCurrentRouteName(CalendarScreen.Month.name)
    }
    @Test
    fun navigate_to_day_from_month_view(){
        composeTestRule.onNodeWithText("1")
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Day.name)
    }

    @Test
    fun navigate_to_create_from_month_view(){
        composeTestRule.onNodeWithText("+")
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Create.name)
    }

    @Test
    fun navigate_to_create_from_day_view() {
        navigateToDayScreen()
//        val backText = composeTestRule.activity.getString(R.string.back)
        composeTestRule.onNodeWithContentDescription("Floating action button")
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Create.name)
    }

    @Test
    fun from_day_view_click_back_button() {
        navigateToDayScreen()
        composeTestRule.activity.runOnUiThread {
            composeTestRule.activity.onBackPressedDispatcher.onBackPressed()
            navController.assertCurrentRouteName(CalendarScreen.Day.name)
        }



    }

    private fun navigateToDayScreen() {
//        val dayText = composeTestRule.activity.getString(R.string.day)
        composeTestRule.onNodeWithText("1")
            .performClick()
    }



}