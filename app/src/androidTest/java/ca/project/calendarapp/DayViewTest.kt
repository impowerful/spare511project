package ca.project.calendarapp

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import ca.project.calendarapp.ui.theme.DayPage
import org.junit.Rule
import org.junit.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DayViewTest {
    @get:Rule
    val rule = createComposeRule()

    @Test
    fun eventIsThere(){
        rule.setContent { DayPage() }
        rule.onNodeWithText("Meeting").assertExists()
        rule.onNodeWithText("Presentation").assertExists()
        rule.onNodeWithText("Lunch").assertExists()
        rule.onNodeWithText("Gym").assertExists()
    }

    @Test
    fun testAddDay(){
        rule.setContent { DayPage() }
        val dateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd")
        rule.onNodeWithText(LocalDateTime.now().format(dateTimeFormatter))
        rule.onNodeWithContentDescription("left arrow").performClick()
        rule.onNodeWithText(LocalDateTime.now().minusDays(1).format(dateTimeFormatter)).assertExists()
    }

    @Test
    fun testMinusDay(){
        rule.setContent { DayPage() }
        val dateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd")
        rule.onNodeWithText(LocalDateTime.now().format(dateTimeFormatter))
        rule.onNodeWithContentDescription("right arrow").performClick()
        rule.onNodeWithText(LocalDateTime.now().plusDays(1).format(dateTimeFormatter)).assertExists()
    }
}