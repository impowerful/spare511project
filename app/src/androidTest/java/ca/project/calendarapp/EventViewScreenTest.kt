package ca.project.calendarapp

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.test.SemanticsNodeInteraction
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import ca.project.calendarapp.ui.theme.EventViewScreen
import ca.project.calendarapp.ui.theme.theme.CalendarAppTheme
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EventViewScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun eventViewScreenTest() {
        // Given
        composeTestRule.setContent {
            CalendarAppTheme {
                Surface(
                    modifier = Modifier.fillMaxWidth(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    EventViewScreen(
                        onEditButtonClicked = { /* Handle edit button click */ },
                        onBackButtonClicked = { /* Handle back button click */ },
                    )
                }
            }
        }

        // When - You can perform interactions or verifications here

        // Then - You can use onNode functions to verify the content
        composeTestRule.onNodeWithText("Back").assertExists()
        composeTestRule.onNodeWithText("edit").assertExists()
        composeTestRule.onNodeWithText("Delete").assertExists()
    }
}
