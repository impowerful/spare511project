package ca.project.calendarapp

import androidx.activity.ComponentActivity
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.ComposeNavigator
import androidx.navigation.testing.TestNavHostController
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    private lateinit var navController: TestNavHostController

    //I mocked and create another App is just so I can test it because the
    //month view was not done and ready to be used
    @Before
    fun setupEventNavHost() {
        composeTestRule.setContent {
            navController = TestNavHostController(LocalContext.current).apply {
                navigatorProvider.addNavigator(ComposeNavigator())
            }
            EventApp(navController = navController)
        }
    }

    @Test
    fun eventAppNavHost_verifyStartDestination() {
        navController.assertCurrentRouteName(CalendarScreen.Event.name)
    }

    @Test
    fun eventAppNavHost_verifyBackNavigationExistEventScreen() {
        val backText = composeTestRule.activity.getString(R.string.back)
        composeTestRule.onNodeWithText(backText).assertExists()
    }

    @Test
    fun eventNavHost_clickBackEvent_navigatesToDayScreen() {
        val backText = composeTestRule.activity.getString(R.string.back)
        composeTestRule.onNodeWithText(backText)
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Day.name)
    }

    @Test
    fun eventNavHost_clickEditEvent_navigatesToEditScreen() {
        val editText = composeTestRule.activity.getString(R.string.edit)
        composeTestRule.onNodeWithText(editText)
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Edit.name)
    }

    @Test
    fun editNavHost_clickBackEdit_navigatesToEventScreen() {
        navigateToEditScreen()
        val backText = composeTestRule.activity.getString(R.string.back)
        composeTestRule.onNodeWithText(backText)
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Event.name)
    }

    @Test
    fun editNavHost_clickSaveEdit_navigatesToEventScreen() {
        navigateToEditScreen()
        val saveText = composeTestRule.activity.getString(R.string.save)
        composeTestRule.onNodeWithText(saveText)
            .performClick()
        navController.assertCurrentRouteName(CalendarScreen.Event.name)
    }
    private fun navigateToEditScreen() {
        val editText = composeTestRule.activity.getString(R.string.edit)
        composeTestRule.onNodeWithText(editText)
            .performClick()
    }
}