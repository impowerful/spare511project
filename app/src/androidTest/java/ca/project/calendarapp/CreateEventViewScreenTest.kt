package ca.project.calendarapp

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import ca.project.calendarapp.ui.theme.CalendarViewModel
import ca.project.calendarapp.ui.theme.CreateEventScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CreateEventScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun createEventScreenRendersCorrectly() {
        // Given
        composeTestRule.setContent {
            CreateEventScreen(
                onCreateButtonClicked = { /* Handle create button click */ },
                onBackButtonClicked = { /* Handle back button click */ },
                calendarViewModel = CalendarViewModel(),
            )
        }

        // Allow time for rendering
        Thread.sleep(2000)

        // Then
        composeTestRule.onNodeWithText("Back").assertExists()
        composeTestRule.onNodeWithText("Create Event").assertExists()
        composeTestRule.onNodeWithText("Title").assertIsDisplayed()
        composeTestRule.onNodeWithText("Location").assertIsDisplayed()
        composeTestRule.onNodeWithText("Description").assertIsDisplayed()
    }
}
