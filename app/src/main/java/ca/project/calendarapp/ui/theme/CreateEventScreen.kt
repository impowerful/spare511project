package ca.project.calendarapp.ui.theme

import android.app.AlertDialog
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import ca.project.calendarapp.R
import ca.project.calendarapp.data.Event
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateEventScreen(
    context: Context = LocalContext.current,
    calendar: Calendar,
    onCreateButtonClicked: () -> Unit,
    onBackButtonClicked: () -> Unit,
    calendarViewModel: CalendarViewModel = viewModel(),
    modifier: Modifier = Modifier
){

    var title by remember { mutableStateOf("Title") }
    var location by remember { mutableStateOf("Location") }
    var description by remember { mutableStateOf("Description") }
    var eventDate by remember { mutableStateOf(SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(Calendar.getInstance().time)) }
    var startTime by remember { mutableStateOf(SimpleDateFormat("HH:mm", Locale.getDefault()).format(Calendar.getInstance().time)) }
    var endTime by remember {
        mutableStateOf(
            SimpleDateFormat("HH:mm", Locale.getDefault()).format(
                Calendar.getInstance().apply { add(Calendar.HOUR_OF_DAY, 1) }.time
            )
        )
    }
    //Set this off default year
    var month by remember { mutableStateOf("") }
    var year by remember { mutableStateOf("") }

    val dateSelector = DateSelector(context, calendar){
        var dateSplit = it.split('/')
        month = dateSplit[0]
        year = dateSplit[2]
        eventDate = it
    }

    dateSelector.datePicker.minDate = calendar.timeInMillis
    
    val startTimeSelector = TimeSelector(context, calendar){
        startTime = it
    }

    val endTimeSelector = TimeSelector(context, calendar){
        endTime = it
    }

    Column(modifier = modifier
        .padding(16.dp)
        .verticalScroll(rememberScrollState())) {
        Button(
            onClick = {
                onBackButtonClicked()
            },
            modifier = modifier.padding(top = 16.dp)
        ) {
            Text( text = stringResource(id = R.string.back))
        }
        Row(modifier = modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween)
        {
            Text(
                text = "Title: ",
                fontWeight = FontWeight.Bold,
                fontSize = 25.sp,
                modifier = Modifier
                    .padding(top = 5.dp, end =10.dp)
            )
            TextField(
                value = title,
                onValueChange = { title = it },
                modifier = Modifier
                    .width(250.dp)
                    .height(59.dp)
                    .padding(end = 30.dp)
            )
        }
        Row(modifier = modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween)
        {
            Text(
                text = "Location: ",
                fontWeight = FontWeight.Bold,
                fontSize = 25.sp,
                modifier = Modifier
                    .padding(top = 5.dp, end =10.dp)
            )
            TextField(
                value = location,
                onValueChange = { location = it },
                modifier = Modifier
                    .width(250.dp)
                    .height(59.dp)
                    .padding(end = 30.dp)
            )
        }
        Row(modifier = modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween)
        {
            Text(
                text = "Description: ",
                fontWeight = FontWeight.Bold,
                fontSize = 25.sp,
                modifier = Modifier
                    .padding(top = 5.dp, end =10.dp)
            )
            TextField(
                value = description,
                onValueChange = { description = it },
                modifier = Modifier
                    .width(250.dp)
                    .height(59.dp)
                    .padding(end = 30.dp)
            )
        }
        //Figure how to fix to show the value in the text field
        //Also create a default date
        SelectorTextField("Date: ", dateSelector , eventDate)
        SelectorTextField("Start Time: ", startTimeSelector, startTime)
        SelectorTextField("End Time: ", endTimeSelector, endTime)
        Button(
            onClick = {
                val event = Event(
                    title,
                    location,
                    description,
                    eventDate,
                    startTime,
                    endTime,
                    month,
                    year,)

                calendarViewModel.addEvent(event)

                onCreateButtonClicked()
            },
            modifier = modifier.padding(top = 16.dp)
        ) {
            Text("Create Event")
        }
    }
}

//@RequiresApi(Build.VERSION_CODES.O)
//@Preview(showBackground = true)
//@Composable
//fun CreateEventScreenPreview() {
//    CalendarAppTheme {
//        CreateEventScreen(
//            onCreateButtonClicked = { },
//            onBackButtonClicked = { }
//        )
//    }
//}