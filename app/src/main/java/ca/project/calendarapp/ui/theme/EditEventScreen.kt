package ca.project.calendarapp.ui.theme

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ca.project.calendarapp.R
import ca.project.calendarapp.data.Event
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class)
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun EditEventScreen(
    context: Context = LocalContext.current,
    onSaveButtonClicked: () -> Unit,
    calendar: Calendar,
    calendarViewModel: CalendarViewModel = viewModel(),
    modifier: Modifier = Modifier
) {
    val calendarUiState by calendarViewModel.uiState.collectAsState()
    var editedEvent by remember { mutableStateOf(calendarUiState.currentEvent) }
    var eventDate by remember { mutableStateOf(editedEvent.eventDate) }
    var startTime by remember { mutableStateOf(editedEvent.startTime) }
    var endTime by remember { mutableStateOf(editedEvent.endTime) }
    var month by remember { mutableStateOf(editedEvent.month) }
    var year by remember { mutableStateOf(editedEvent.month) }

    val dateSelector = DateSelector(context, calendar){
        var dateSplit = it.split('/')
        editedEvent.copy(month = dateSplit[0])
        editedEvent.copy(year = dateSplit[2])
        eventDate = it
    }

    dateSelector.datePicker.minDate = calendar.timeInMillis

    val startTimeSelector = TimeSelector(context, calendar){
        startTime = it
    }

    val endTimeSelector = TimeSelector(context, calendar){
        endTime = it
    }

    Column(modifier = modifier
        .padding(16.dp)
        .verticalScroll(rememberScrollState())) {
        Button(
            onClick = { onSaveButtonClicked() },
            modifier = modifier.padding(top = 16.dp)
        ) {
            Text( text = stringResource(id = R.string.back))
        }

        TextField(
            value = editedEvent.title,
            onValueChange = { editedEvent = editedEvent.copy(title = it) },
            modifier = modifier.fillMaxWidth(),
            placeholder = { Text("Title") }
        )


        TextField(
            value = editedEvent.location,
            onValueChange = { editedEvent = editedEvent.copy(location = it) },
            modifier = modifier.fillMaxWidth(),
            placeholder = { Text("Location") }
        )

        TextField(
            value = editedEvent.description,
            onValueChange = { editedEvent = editedEvent.copy(description = it) },
            modifier = Modifier.fillMaxWidth(),
            placeholder = { Text("Description") }
        )

        SelectorTextField("Date: ", dateSelector , eventDate)
        SelectorTextField("Start Time: ", startTimeSelector, startTime)
        SelectorTextField("End Time: ", endTimeSelector, endTime)

        Button(
            onClick = {
                val updateEvent = Event(
                    editedEvent.title,
                    editedEvent.location,
                    editedEvent.description,
                    eventDate,
                    startTime,
                    endTime,
                    editedEvent.month,
                    editedEvent.year,
                    editedEvent.id,
                )
                calendarViewModel.updateEvent(editedEvent.id, updatedEvent = updateEvent)
                onSaveButtonClicked()
            },
            modifier = modifier.padding(top = 16.dp)
        ) {
            Text(text = stringResource(id = R.string.save))
        }
    }
}
//@RequiresApi(Build.VERSION_CODES.O)
//@Preview(showBackground = true)
//@Composable
//fun EditEventScreenPreview() {
//    CalendarAppTheme {
//        EditEventScreen(onSaveButtonClicked = { })
//    }
//}