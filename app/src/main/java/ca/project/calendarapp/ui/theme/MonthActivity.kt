package ca.project.calendarapp.ui.theme

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import java.text.DateFormatSymbols
import java.util.Calendar
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.FloatingActionButton
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ca.project.calendarapp.ui.theme.theme.CalendarAppTheme


//Get all the days in a month
fun getMonthDays(month : Int) : List<Int>{
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.MONTH, month)
    val days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

    return (1..days).toList()
}

@Composable
fun MonthPage(
    onCreateButtonClicked : () -> Unit = {},
    onDayButtonClicked : () -> Unit = {},
    modifier: Modifier = Modifier
) {

    var monthIndex by remember { mutableStateOf(0) }
    val monthList = getMonthDays(monthIndex)
    val displayMonths = DateFormatSymbols().months

    var weekDays = listOf<String>("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
    var currentWeek = 1
    var numOfWeeks = 0

    //Get the number of days through first day of the week
    monthList.forEach{
        if ( it % 7 == 1){
            numOfWeeks += 1
        }
    }


    Column {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(horizontal = 80.dp),
            verticalAlignment = Alignment.CenterVertically
        ){
            Button(onClick = {
                if(monthIndex > 0){
                    monthIndex -= 1
                }
            }) {
                Text(text = "<")
            }
            Text(
                text = displayMonths[monthIndex],
                modifier = modifier.padding(4.dp)
            )

            Button(onClick = {
                if(monthIndex < displayMonths.size - 1){
                    monthIndex += 1
                }
            }) {
                Text(text = ">")
            }
        }


        Spacer(modifier = modifier)

        //List the days of the week
        Row(
            modifier = modifier.fillMaxWidth()
        ){

            weekDays.forEach{
                Card(
                    modifier = modifier
                        .width(50.dp)
                        .height(40.dp)
                        .padding(3.dp)
                ) {
                    Text(
                        text = "" + it,
                        textAlign = TextAlign.Center,
                        modifier = modifier
                            .fillMaxSize()
                            .background(Color.LightGray))
                }
            }
        }

        //For every week in month
        for( week in 1..numOfWeeks){
            Row {

                //For every day in october
                monthList.forEach{

                    //For every day in a week
                    for (n in currentWeek..7*week) {
                        if(it == n){

                            Card(
                                modifier = Modifier
                                    .clickable { onDayButtonClicked() }
                                    .size(50.dp)
                                    .background(Color.Blue)
                            ) {
                                Box(
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .padding(8.dp)
                                ) {
                                    Text(
                                        text = "$n",
                                        modifier = Modifier.fillMaxSize(),
                                        fontSize = 16.sp,
                                        textAlign = TextAlign.Center
                                    )
                                }
                            }
                        }
                    }
                }
                currentWeek += 7
            }
        }

        Spacer(modifier = modifier.padding(vertical = 4.dp))

        FloatingActionButton(
            onClick = { onCreateButtonClicked() },
            modifier = modifier.padding(horizontal = 140.dp),

        ) {
            Text(
                text = "+",

            )
        }


    }
}

@Preview(showBackground = true)
@Composable
fun MonthPreview() {
    CalendarAppTheme {
        MonthPage()
    }
}
