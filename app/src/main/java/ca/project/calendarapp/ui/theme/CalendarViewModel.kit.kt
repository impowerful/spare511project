package ca.project.calendarapp.ui.theme

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ca.project.calendarapp.data.Event
import ca.project.calendarapp.data.EventsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class CalendarViewModel(private val eventsRepository: EventsRepository) : ViewModel() {
    private val _uiState = MutableStateFlow(CalendarUiState())
    val uiState: StateFlow<CalendarUiState> get() = _uiState.asStateFlow()

    private val coroutineScope = CoroutineScope((Dispatchers.Main))

    fun addEvent(event: Event) {
        viewModelScope.launch(Dispatchers.IO) {
            eventsRepository.insertEvent(event)
            _uiState.value = _uiState.value.copy(currentEvent = event)
        }
    }

    fun updateEvent(eventId: Int, updatedEvent: Event) {
        coroutineScope.launch(Dispatchers.IO) {
            eventsRepository.updateEvent(updatedEvent)
            _uiState.value = _uiState.value.copy(currentEvent = updatedEvent)
            _uiState.value = _uiState.value.copy(allEvents = _uiState.value.allEvents.apply {
                val index = indexOfFirst { it.id == eventId }
                if (index != -1) {
                    set(index, updatedEvent)
                }
            })
            _uiState.value = _uiState.value.copy(dayEvents = _uiState.value.dayEvents.apply {
                val index = indexOfFirst { it.id == eventId }
                if (index != -1) {
                    set(index, updatedEvent)
                }
            })
            _uiState.value = _uiState.value.copy(monthEvents = _uiState.value.monthEvents.apply {
                val index = indexOfFirst { it.id == eventId }
                if (index != -1) {
                    set(index, updatedEvent)
                }
            })
        }
    }

    fun deleteEvent(event: Event, eventId: Int) {
        coroutineScope.launch(Dispatchers.IO) {
            eventsRepository.deleteEvent(event)
            _uiState.value = _uiState.value.copy(allEvents = _uiState.value.allEvents.toMutableList().apply {
                val eventToRemove = _uiState.value.allEvents.find { it.id == eventId }
                remove(eventToRemove)
            })
            _uiState.value = _uiState.value.copy(dayEvents = _uiState.value.dayEvents.toMutableList().apply {
                val eventToRemove = _uiState.value.dayEvents.find { it.id == eventId }
                remove(eventToRemove)
            })
            _uiState.value = _uiState.value.copy(monthEvents = _uiState.value.monthEvents.toMutableList().apply {
                val eventToRemove = _uiState.value.monthEvents.find { it.id == eventId }
                remove(eventToRemove)
            })
        }
    }

    //Get Event
    fun getEvent(id: Int) {
        coroutineScope.launch(Dispatchers.IO) {
            try{
                val event = eventsRepository.getEventStream(id).first()
                _uiState.value = _uiState.value.copy(currentEvent = event)
            } catch (e: Exception) {
                // Handle exceptions if necessary
            }
        }
    }

    //Get Event For Day
    fun getEventByDay(date: String) {
        coroutineScope.launch(Dispatchers.IO) {
            try{
                eventsRepository.getEventsByDayStream(date).collect { dbEvent ->
                    val mutableList: MutableList<Event> = dbEvent.toMutableList()
                    _uiState.value = _uiState.value.copy(dayEvents = mutableList)
                }
            } catch (e: Exception) {
                // Handle exceptions if necessary
            }
        }
    }

    //Get Events For Month
    fun getEventsByMonth(date: String) {
        coroutineScope.launch(Dispatchers.IO) {
            try{
                val dateSplit = date.split('/')
                val month = dateSplit[0]
                val year = dateSplit[2]
                eventsRepository.getEventsByMonthStream(month, year).collect { dbEvent ->
                    val mutableList: MutableList<Event> = dbEvent.toMutableList()
                    _uiState.value = _uiState.value.copy(monthEvents = mutableList)
                }
            } catch (e: Exception) {
                // Handle exceptions if necessary
            }
        }
    }
}
