package ca.project.calendarapp.ui.theme

import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import androidx.activity.compose.BackHandler
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.KeyboardArrowLeft
import androidx.compose.material.icons.rounded.KeyboardArrowRight
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material3.Button
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp
import ca.project.calendarapp.CalendarScreen
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DayPage(
    onEventButtonClicked : () -> Unit = {},
    onAddEventButtonClicked : () -> Unit = {},
    onBackButton : () -> Unit = {},
    modifier: Modifier = Modifier
){
    BackHandler(
        onBack = { onBackButton() }
    )
    Column {
        Head(
            modifier = modifier
        )
        HoursList(
            onEventButtonClicked = { onEventButtonClicked() },
            modifier = Modifier
        )
    }
    FloatingActionButton(
        onClick = { onAddEventButtonClicked() },
        modifier = Modifier
            .padding(16.dp)
            //need fix for this, should not be hardcoded
            .offset(x = 325.dp,y = 25.dp)
    ) {
        Icon(Icons.Rounded.Add, "Floating action button")
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Head(
    modifier : Modifier = Modifier
){
    val dateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd")
    var selectedDate = LocalDateTime.now()
    var mutableDate by remember { mutableStateOf( selectedDate.format(dateTimeFormatter) ) }
    Row(
        modifier
            .height(40.dp)
            .fillMaxWidth()
            .background(color = Color.LightGray)
    ) {
        Icon(
            //onclick to change to prev day
            Icons.Rounded.KeyboardArrowLeft,
            contentDescription = "left arrow",
            modifier
                .weight(1f)
                .clickable {
                    selectedDate = selectedDate.minusDays(1)
                    mutableDate = selectedDate.format(dateTimeFormatter)
                }
        )
        Text(
            //date from view model
            text = mutableDate,
            modifier.weight(1f)
        )
        Icon(
            //onclick to change to next day
            Icons.Rounded.KeyboardArrowRight,
            contentDescription = "right arrow",
            modifier
                .weight(1f)
                .clickable {
                    selectedDate = selectedDate.plusDays(1)
                    mutableDate = selectedDate.format(dateTimeFormatter)
                }
        )
    }
}
data class EventMock(val title: String, val startTime: LocalDateTime, val endTime: LocalDateTime)
// Composable function to display events in the day view
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun HoursList(
    onEventButtonClicked : () -> Unit = {},
    modifier: Modifier = Modifier
) {
    val events = listOf(
        //need to change this to use localDateTime
        EventMock("Meeting",
            LocalDateTime.now().withHour(8).withMinute(0),
            LocalDateTime.now().withHour(9).withMinute(0)),
        EventMock("Lunch",
            LocalDateTime.now().withHour(12).withMinute(30),
            LocalDateTime.now().withHour(13).withMinute(0)),
        EventMock("Presentation",
            LocalDateTime.now().withHour(10).withMinute(30),
            LocalDateTime.now().withHour(11).withMinute(30)),
        EventMock("Gym",
            LocalDateTime.now().withHour(17).withMinute(0),
            LocalDateTime.now().withHour(20).withMinute(0)),
    )
    val state = rememberScrollState()
    val sortedEvents = events.sortedBy { it.startTime }
    val sdf = DateTimeFormatter.ofPattern("HH:mm")
    val timesOfDay: MutableList<LocalDateTime> = mutableListOf()
    val dayStart = LocalDateTime.now().withHour(0).withMinute(0)
    val dayEnd = LocalDateTime.now().withHour(23).withMinute(59)
    var currentTime = LocalDateTime.now().withHour(0).withMinute(0)

    //set up first column of timeslots
    while (currentTime.isBefore(dayEnd)) {
        val endTime = currentTime.plusHours(1)

        timesOfDay.add(currentTime)

        currentTime = endTime
    }
    var startPos: Float = 0F
    Box {
        LazyRow(
            modifier
                .verticalScroll(state)
                .height((24 * 60).toFloat().dp)
                .fillMaxWidth()
        ) {
            item {
                LazyColumn(
                    modifier = Modifier
                        .background(color = Color.LightGray)
                        .fillParentMaxWidth(0.15f)
                ) {
                    items(timesOfDay) { dateTime ->
                        Text(
                            text = dateTime.format(sdf),
                            modifier.height(60.dp)
                        )
                    }
                }
            }
            item {
                LazyColumn(
                    modifier = Modifier
                        .background(color = Color.LightGray)
                        .fillParentMaxWidth(0.85f)
                        .height((24 * 60).toFloat().dp)
                ) {
                    items(sortedEvents) { event ->
                        val pad = (((event.startTime.hour)
                            .toFloat() * 60) + (event.startTime.minute
                            .toFloat())) - (startPos)
                        var height = event.startTime.until(event.endTime, java.time.temporal.ChronoUnit.MINUTES)
                            .toFloat()
                        // the if statements here are to account for events that start during other events
                        if (pad + height > 0) {
                            if (pad <= 0) {
                                height = (pad + height) - startPos
                            }
                            Button(
                                onClick = { onEventButtonClicked() },
                                shape = RectangleShape,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(top = pad.dp)
                                    .height(height.dp)
                            ) {
                                Text(
                                    text = event.title
                                )
                            }
                            startPos += (pad + height)
                        }
                    }
                }
            }
        }
    }

}

