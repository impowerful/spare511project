package ca.project.calendarapp.ui.theme

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ca.project.calendarapp.CalendarScreen
import ca.project.calendarapp.R
import ca.project.calendarapp.ui.theme.theme.CalendarAppTheme

@Composable
fun EventViewScreen(
    onEditButtonClicked: () -> Unit,
    onBackButtonClicked: () -> Unit,
    calendarViewModel: CalendarViewModel = viewModel(),
    modifier: Modifier = Modifier
) {
    val calendarUiState by calendarViewModel.uiState.collectAsState()
    val event = calendarUiState.currentEvent

    Column(
        modifier = modifier.padding(16.dp)
    ) {
        Button(
            onClick = onBackButtonClicked,
            modifier = modifier.padding(top = 16.dp)
        ) {
            Text( text = stringResource(id = R.string.back))
        }

        Text(text = "Title: ${event.title}", fontWeight = FontWeight.Bold)
        Text(text = "Location: ${event.location}")
        Text(text = "Description: ${event.description}")
        Text(text = "Event Date: ${event.eventDate}")
        Text(text = "Start Time: ${event.startTime}")
        Text(text = "End Time: ${event.endTime}")

        Row(
            modifier = modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            // Edit button
            Button(
                onClick = onEditButtonClicked,
                modifier = modifier.padding(top = 16.dp)
            ) {
                Text( text = stringResource(id = R.string.edit))
            }

            // Delete button
            Button(
                onClick = {
                    calendarViewModel.deleteEvent(event , event.id)
                },
                modifier = modifier.padding(top = 16.dp)
            ) {
                Text("Delete")
            }
        }
    }
}


@RequiresApi(Build.VERSION_CODES.O)
@Preview(showBackground = true)
@Composable
fun EventViewScreen() {
    CalendarAppTheme {
        EventViewScreen(
            onEditButtonClicked = { },
            onBackButtonClicked = { },
        )
    }
}
