package ca.project.calendarapp.ui.theme

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import ca.project.calendarapp.data.Event

data class CalendarUiState(
    var allEvents: MutableList<Event> = mutableListOf(),
    var monthEvents: MutableList<Event> = mutableListOf(),
    var dayEvents: MutableList<Event> = mutableListOf(),
    var currentEvent: Event = Event(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    )
)
