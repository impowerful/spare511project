package ca.project.calendarapp.data
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface EventsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(Event: Event)

    @Update
    suspend fun update(Event: Event)

    @Delete
    suspend fun delete(Event: Event)

    @Query("SELECT * from events WHERE id = :id")
    fun getEvent(id: Int): Flow<Event>

    @Query("SELECT * from events WHERE eventDate = :date")
    fun getEventsByDay(date: String): Flow<List<Event>>
    @Query("SELECT * from events WHERE month = :month AND year =:year")
    fun getEventsByMonth(month: String, year: String): Flow<List<Event>>
}