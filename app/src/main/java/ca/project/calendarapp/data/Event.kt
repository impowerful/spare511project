package ca.project.calendarapp.data

import java.time.LocalDateTime
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.time.LocalDate
import java.time.LocalTime

//this will be changed to Event
//Change for ID to be auto generated
@Entity(tableName = "events")
@TypeConverters(Converters::class)
data class Event(
    var title: String = "",
    var location: String = "",
    var description: String = "",
    var eventDate: String = "",
    var startTime: String = "",
    var endTime: String = "",
    var month: String = "",
    var year: String = "",
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
    )