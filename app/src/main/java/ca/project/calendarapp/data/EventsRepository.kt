package ca.project.calendarapp.data

import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

interface EventsRepository {
    /**
     * Retrieve all the events from the the given data source.
     */
    fun getEventsByDayStream(date: String): Flow<List<Event>>

    /**
     * Retrieve all the events from the the given data source.
     */
    fun getEventsByMonthStream(month: String, year: String): Flow<List<Event>>

    /**
     * Retrieve an event from the given data source that matches with the [id].
     */
    fun getEventStream(id: Int): Flow<Event>

    /**
     * Insert event in the data source
     */
    suspend fun insertEvent(item: Event)

    /**
     * Delete event from the data source
     */
    suspend fun deleteEvent(item: Event)

    /**
     * Update event in the data source
     */
    suspend fun updateEvent(item: Event)
}