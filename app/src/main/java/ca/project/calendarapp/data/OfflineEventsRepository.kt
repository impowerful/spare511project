package ca.project.calendarapp.data
import kotlinx.coroutines.flow.Flow


class OfflineEventsRepository(private val EventsDao: EventsDao) : EventsRepository {
    override fun getEventsByDayStream(date: String): Flow<List<Event>> = EventsDao.getEventsByDay(date)
    override fun getEventsByMonthStream(month: String, year: String): Flow<List<Event>> = EventsDao.getEventsByMonth(month, year)

    override fun getEventStream(id: Int): Flow<Event> = EventsDao.getEvent(id)

    override suspend fun insertEvent(Event: Event) = EventsDao.insert(Event)

    override suspend fun deleteEvent(Event: Event) = EventsDao.delete(Event)

    override suspend fun updateEvent(Event: Event) = EventsDao.update(Event)

}