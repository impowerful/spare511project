package ca.project.calendarapp

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import ca.project.calendarapp.data.InventoryDatabase
import ca.project.calendarapp.data.OfflineEventsRepository
import ca.project.calendarapp.ui.theme.CalendarViewModel
import ca.project.calendarapp.ui.theme.CreateEventScreen
import ca.project.calendarapp.ui.theme.DayPage
import ca.project.calendarapp.ui.theme.EditEventScreen
import ca.project.calendarapp.ui.theme.EventViewScreen
import ca.project.calendarapp.ui.theme.MonthPage
import ca.project.calendarapp.ui.theme.theme.CalendarAppTheme
import java.util.Calendar

enum class CalendarScreen(@StringRes val title: Int) {
    Month(title = R.string.month),
    Day(title = R.string.day),
    Event(title = R.string.event),
    Edit(title = R.string.edit),
    Create(title = R.string.create),
}

class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalendarAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val db = InventoryDatabase.getDatabase(applicationContext)
                    val eventDao = db.eventDao()
                    MainApp(offlineEventsRepository = OfflineEventsRepository(eventDao))
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainApp(
    offlineEventsRepository: OfflineEventsRepository,
    calendarViewModel: CalendarViewModel = CalendarViewModel(offlineEventsRepository),
    navController: NavHostController = rememberNavController(),
    modifier: Modifier = Modifier
) {
    val backStackEntry by navController.currentBackStackEntryAsState()
    val currentScreen = CalendarScreen.valueOf(
        backStackEntry?.destination?.route ?: CalendarScreen.Month.name
    )
    Scaffold(

    ) { innerPadding ->
        //example of view model stuff to hold state
//        val uiState by viewModel.uiState.collectAsState()
//        if (uiState.food == ""){
//            viewModel.getRandomFood(viewModelList.newList)
//        }
//        val uiStateList by viewModelList.uiState.collectAsState()
        NavHost(
            navController = navController,
            startDestination = CalendarScreen.Month.name,
            modifier = Modifier.padding(innerPadding)
        ) {

            composable(route = CalendarScreen.Month.name) {
                MonthPage(
                    onCreateButtonClicked = { navController.navigate(CalendarScreen.Create.name) },
                    onDayButtonClicked = { navController.navigate(CalendarScreen.Day.name) },
                    modifier = modifier
                )
            }
//            composable(route = CalendarScreen.Month.name) {
//                Column {
//                    Text(
//                        text = "Month",
//                        modifier = modifier
//                    )
//                    Button(onClick = { navController.navigate(CalendarScreen.Day.name) }) {
//                        Text(text = "Day view")
//                    }
//                }
                // an example of what will actually go here
                // in this example you create a file FoodList.kt with a FoodListPage composable
//                FoodListPage(
//                    onSaveButtonClicked = { navController.navigate(FoodScreen.Start.name) },
//                    viewModel = viewModelList,
//                    state = uiStateList,
//                    modifier = Modifier.fillMaxHeight()
//                )
//            }
            composable(route = CalendarScreen.Day.name) {
                DayPage(
                    onAddEventButtonClicked = { navController.navigate(CalendarScreen.Create.name) },
                    onEventButtonClicked = { navController.navigate(CalendarScreen.Event.name) },
                    onBackButton = { navController.navigate(CalendarScreen.Month.name) },
                    modifier = modifier
                )
            }
            composable(route = CalendarScreen.Event.name) {
                EventViewScreen(
                    onEditButtonClicked = { navController.navigate(CalendarScreen.Edit.name) },
                    onBackButtonClicked = { navController.navigate(CalendarScreen.Day.name) },
                    calendarViewModel = calendarViewModel,
                    modifier = modifier
                )
            }

            composable(route = CalendarScreen.Edit.name) {
                EditEventScreen(
                    calendar = Calendar.getInstance(),
                    onSaveButtonClicked = { navController.navigate(CalendarScreen.Event.name) },
                    calendarViewModel = calendarViewModel,
                    modifier = modifier
                )
            }
            //Pass a calendar
            composable(route = CalendarScreen.Create.name) {
                CreateEventScreen(
                    calendar = Calendar.getInstance(),
                    onCreateButtonClicked = { navController.navigate(CalendarScreen.Event.name) },
                    onBackButtonClicked = { navController.navigate(CalendarScreen.Month.name) },
                    calendarViewModel = calendarViewModel,
                    modifier = modifier
                )
            }
        }
    }

}

//@RequiresApi(Build.VERSION_CODES.O)
//@Preview(showBackground = true)
//@Composable
//fun GreetingPreview() {
//    CalendarAppTheme {
//        MainApp()
//    }
//}